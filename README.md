##Timothy Carver
##LIS4369
1. Backward-engineer (using Visual Studio Community Edition) the following console
application screenshot:
2. Requirements (see screenshot for default values and program function):
a. Display short assignment requirements.
b. Display *your* name as “author.”
c. Display current date/time (must include date/time, your format preference).
d. Create three classes: (see fields and methods below)
a. Base class Product:
1. Create two contructors:
a. default constructor
b. constructor w/default parameter values
2. Create three properties:
a. Code (string)
b. Description (string)
c. Price (decimal)
Note: Code and Description are auto-implemented properties
3. Create two methods:
a. GetObjectInfo(), and
b. GetObjectInfo(arg), (overloaded) that passes a string
arg to be used as a separator for display purposes
b. Derived class Software:
1. Create two contructors:
a. default constructor (calls base class implicitly)
b. constructor w/default parameter values (calls base class
constructor explicitly)
2. Create one property: Version (string)
3. Create overridden method (polymorphism): GetObjectInfo(arg)
c. Derived class Book:
1. Create two contructors:
a. default constructor (calls base class implicitly)
b. constructor w/default parameter values (calls base class
constructor explicitly)
2. Create one property: Author (string)
3. Create overridden method (polymorphism): GetObjectInfo(arg)
Note: There are *no* auto-implemented properties in the derived classes.
# Assignment 5 
![A5_1.png](https://bitbucket.org/repo/o4njXd/images/198797928-A5_1.png)
![A5_2.png](https://bitbucket.org/repo/o4njXd/images/1998567916-A5_2.png)